import gym
import gym_uav


if __name__ == "__main__":
    env = gym.make('uav-v0')
    env.reset()
    for i in range(1000):
        action = env.action_space.sample()
        obs, rew, done, info = env.step(action)
        env.render()

        if done:
            break