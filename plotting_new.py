import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':

    environment = ['uav-v0']
    # traj_limt = ['tra_lim=1', 'tra_lim=100', 'tra_lim=20', 'tra_lim=500']
    traj_limt = ['tra_lim=100', 'tra_lim=500']
    demo_type = ['exp_pat=uav-', 'exp_pat=uav_wrong']
    path = r'c:\d\logs_ddpgfd-master'
    seed = ['1', '2', '3']
    variance = ['var=0.0', 'var=0.4']

    plot_type = 'success'

    if plot_type == 'success':
        for e in environment:
            for d in demo_type:
                for t in traj_limt:
                    log_set = []
                    for v in variance:
                        log_set_sub = []
                        for file in os.listdir(path):
                            if d in file and e in file and t in file and v in file:
                                label = ''
                                for s in seed:
                                    if s in file:
                                        label = s
                                log_set_sub.append([os.path.join(path, file, 'log.txt'), label])
                        log_set.append([log_set_sub, v])

                    print(log_set)
                    # exit(0)
                    figsize = 8, 7
                    plt.figure(figsize=figsize)
                    plt.rcParams.update({'font.size': 20, 'font.family': 'serif', 'font.serif': 'Times New Roman'})
                    plt.gca().yaxis.get_major_formatter().set_powerlimits((0, 1))
                    plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 1))

                    for f in range(len(log_set)):
                        data_first = log_set[f][0]
                        label_first = log_set[f][1]
                        success_rates_total = []
                        training_steps_total = []
                        for g in range(len(data_first)):
                            data_second = open(data_first[g][0])
                            label_second = data_first[g][1]
                            line = data_second.readline()
                            training_steps = []
                            success_rates = []
                            episode_return = []
                            while line:
                                if 'return-average' in line and 'd-return-average' not in line:
                                    try:
                                        reward = float(line.split('|')[2])
                                        success_rates.append(reward)
                                    except:
                                        pass
                                if 'total-samples' in line:
                                    try:
                                        step = int(line.split('|')[2])
                                        training_steps.append(step)
                                    except:
                                        pass
                                line = data_second.readline()

                            success_rates_total.append(success_rates)
                            training_steps_total.append(training_steps)

                        selected_success_rates = []
                        selected_training_steps = []

                        for i in range(len(success_rates_total)):
                            if len(success_rates_total[i]) <= 100:
                                pass
                            else:
                                selected_success_rates.append(success_rates_total[i][0:100])
                                selected_training_steps.append(training_steps_total[i][0:100])
                        if selected_success_rates:
                            print("total selected seeds", len(selected_success_rates))
                            success_rates = np.array(selected_success_rates)
                            success_rates = np.mean(success_rates, 0)
                            training_steps = selected_training_steps[0]
                            plt.plot(training_steps, success_rates, linewidth=3.0)

                    print("shape check", np.shape(training_steps), np.shape(success_rates), type(training_steps),
                          type(success_rates))
                    data = np.concatenate(
                        (np.expand_dims(np.array(training_steps), 0), np.expand_dims(success_rates, 0)), 0)
                    name = 'POfD_' + "uav" + '.npy'
                    np.save(name, data)

                    plt.xlabel('training step')
                    plt.ylabel('success rate')

                    plt.legend()
                    plt.grid()
                    # plt.title(title)
                    plt.show()
                    plt.close()
