#!/bin/bash
export CUDA_VISIBLE_DEVICES=$1
echo $CUDA_VISIBLE_DEVICES
SEEDS="0 1 2 3 4"
for SEED in $SEEDS
do
    python baselines/run.py --alg=ddpg --env=Humanoid-v1 --expert_path='dataset/humanoid.npz' --eval_freq=20 --num_epochs 4000 --traj_limitation 240 --seed $SEED
done