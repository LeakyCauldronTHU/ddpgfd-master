import gym
import gym_uav
import torch
import numpy as np
from torch.autograd import Variable

# 'acs', 'ep_rets', 'rews', 'obs'


class Config:
    def __init__(self):
        self.mode = 'train'
        self.env = 'uav-v0'
        self.hidden1 = 400
        self.hidden2 = 300
        self.ou_sigma = 0.2
        self.ou_mu = 0.0
        self.ou_theta = 0.15
        self.epsilon = 50000
        self.seed = -1
        self.init_w = 0.003


class Policy_Domain:
    def __init__(self, observation_space, action_space):
        self.config = Config()
        self.current_direct_wrong = 'north'
        self.min_distance_x = 50.0
        self.min_distance_y = 50.0

    def forward(self, state, time_step=1, demo_type='uav', variance=0.00, prior_decay=0.005, reset_flag=False):
        if demo_type == 'uav':
            coefs = [variance, variance]
            time_step = torch.Tensor([time_step])[0]
            perspective = torch.atan(state[12] / state[13])
            first_perspective = torch.where(state[13] > 0,
                                            torch.where(state[12] > 0, perspective / np.pi * 180.0,
                                                        (perspective + 2 * np.pi) / np.pi * 180.0),
                                            (perspective + np.pi) / np.pi * 180.0)

            target = torch.atan(state[10] / state[11])
            position_target = torch.where(state[11] > 0,
                                          torch.where(state[10] > 0, target / np.pi * 180.0,
                                                      (target + 2 * np.pi) / np.pi * 180.0),
                                          (target + np.pi) / np.pi * 180.0)

            first_target = torch.remainder(first_perspective - position_target, 360.0)

            average_direction = torch.where(
                torch.sign(180.0 - first_target) + 1.0 > 0, -first_target / 180.0, (360.0 - first_target) / 180.0)
            variance_direction = 0.0 * average_direction + coefs[0]  # 0.1

            turning_free = torch.where(
                torch.sign(4 - torch.argmin(state[0:9]).float()) + 1.0 > 0, 45.0 + 0 * average_direction,
                -45.0 + 0 * average_direction)
            average_free = turning_free / 180.0
            variance_free = 0.0 * average_free + coefs[0]  # 0.1

            average_steer = torch.where(
                torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0, average_direction, average_free)
            variance_steer = torch.where(
                torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0, variance_direction, variance_free)

            speed = state[14]
            average_throttle = torch.clamp(2.5 - 50 * (speed / 2 + 0.5), -0.5, 0.5)

            variance_throttle = 0.0 * average_throttle + coefs[1]

            # decay = (torch.log(1.0+0.0001*time_step)*4+1).pow(2)

            decay = prior_decay * (time_step - 1) + 1

            covariance = torch.cat(
                (variance_steer.unsqueeze_(0), variance_throttle.unsqueeze_(0)), 0) * decay

            average = torch.cat((average_steer.unsqueeze_(0), average_throttle.unsqueeze_(0)), 0)

        elif demo_type == 'uav_wrong':
            if reset_flag:
                self.current_direct_wrong = 'north'
                self.min_distance_x = 50.0
                self.min_distance_y = 50.0

            coefs = [variance, variance]

            time_step = torch.Tensor([time_step])[0]
            perspective = torch.atan(state[12] / state[13])
            first_perspective = torch.where(state[13] > 0,
                                            torch.where(state[12] > 0, perspective / np.pi * 180.0,
                                                        (perspective + 2 * np.pi) / np.pi * 180.0),
                                            (perspective + np.pi) / np.pi * 180.0)

            target = torch.atan(state[10] / state[11])
            position_target = torch.where(state[11] > 0,
                                          torch.where(state[10] > 0, target / np.pi * 180.0,
                                                      (target + 2 * np.pi) / np.pi * 180.0),
                                          (target + np.pi) / np.pi * 180.0)

            distance = (state[9] / 2 + 0.5) * (torch.sqrt(torch.Tensor([2])[0]) * 3000)

            distance_y = torch.abs(distance * torch.sin(2 * position_target / 360 * torch.Tensor([np.pi])[0]))
            distance_x = torch.abs(distance * torch.cos(2 * position_target / 360 * torch.Tensor([np.pi])[0]))

            # print("distance", distance, distance_x, distance_y)
            if distance_y > self.min_distance_y:
                self.current_direct_wrong = 'north'
            elif distance_x > self.min_distance_x:
                if self.current_direct_wrong == 'north':
                    self.min_distance_x -= 5
                self.current_direct_wrong = 'east'
            else:
                if self.current_direct_wrong == 'east':
                    self.min_distance_y -= 5
                self.current_direct_wrong = 'north'

            if self.current_direct_wrong == 'north':
                if position_target > 0 and position_target < 180:
                    position_target = 90
                else:
                    position_target = 270

            else:
                if position_target < 90 or position_target > 270:
                    position_target = 0
                else:
                    position_target = 180

            # print("orient and value", self.current_direct_wrong, position_target)

            first_target = torch.remainder(first_perspective - position_target, 360.0)

            average_direction = torch.where(
                torch.sign(180.0 - first_target) + 1.0 > 0, -first_target / 180.0, (360.0 - first_target) / 180.0)
            variance_direction = 0.0 * average_direction + coefs[0]  # 0.1

            turning_free = torch.where(
                torch.sign(4 - torch.argmin(state[0:9]).float()) + 1.0 > 0, 45.0 + 0 * average_direction,
                -45.0 + 0 * average_direction)

            average_free = turning_free / 180.0
            variance_free = 0.0 * average_free + coefs[0]  # 0.1

            # distance = (torch.log(2 / (state[9] + 1) - 1)) / (-0.002)
            # average_steer = torch.where(
            #     torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0 or distance<=15, average_direction, average_free)
            # variance_steer = torch.where(
            #     torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0 or distance<=15, variance_direction, variance_free)

            average_steer = torch.where(
                torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0, average_direction, average_free)
            variance_steer = torch.where(
                torch.sign(100 * torch.min(state[0:9]) - 15.0) + 1.0 > 0, variance_direction, variance_free)

            speed = state[14]
            average_throttle = torch.clamp(2.5 - 50 * (speed / 2 + 0.5), -0.5, 0.5)

            variance_throttle = 0.0 * average_throttle + coefs[1]

            # decay = (torch.log(1.0+0.0001*time_step)*4+1).pow(2)

            decay = prior_decay * (time_step - 1) + 1

            covariance = torch.cat(
                (variance_steer.unsqueeze_(0), variance_throttle.unsqueeze_(0)), 0) * decay
            average = torch.cat((average_steer.unsqueeze_(0), average_throttle.unsqueeze_(0)), 0)
        else:
            pass

        return average, covariance

    def action_sample(self, state, time_step, args):
        average, covariance = self.forward(state, time_step, args)
        eps = torch.Tensor(np.random.normal(0, 1, average.shape))
        action = average + eps * covariance.sqrt()
        return action


if __name__ == '__main__':
    env = gym.make('uav-v0')
    max_episode_length = 2000

    # demo_type = 'uav_wrong'
    demo_type = 'uav'

    actions_traj = []
    states_traj = []
    next_states_traj = []
    rewards_traj = []
    dones_traj = []
    returns_traj = []

    prior = Policy_Domain(env.observation_space, env.action_space)
    counter = 0
    for traj in range(1000):
        print("generation trajectory", traj)
        states = []
        next_states = []
        actions = []
        rewards = []
        dones = []
        returns = 0
        obs = env.reset()
        for i in range(max_episode_length):
            # print("iteration", i)
            states.append(obs)
            # action = env.action_space.sample()
            mu, sigma = prior.forward(Variable(torch.Tensor(obs)), time_step=1, demo_type=demo_type)
            eps = torch.randn(mu.size())
            eps = Variable(eps)
            action = (mu + sigma.sqrt() * eps).data
            action = action.cpu().numpy()
            # print("action", action)
            obs, reward, done, info = env.step(action)
            # env.render()
            next_states.append(obs)
            actions.append(action)
            rewards.append(reward)
            dones.append(done)
            returns += reward
            if done:
                counter += 1
                print("counter={}".format(counter))
                obs = env.reset()

        actions_traj.append(np.array(actions))
        states_traj.append(np.array(states))
        next_states_traj.append(np.array(next_states))
        rewards_traj.append(np.array(rewards))
        dones_traj.append(np.array(dones))
        returns_traj.append(np.array([returns]))

    np.savez('./'+demo_type+'.npz', acs=actions_traj, obs=states_traj, rews=rewards_traj, next_obs=next_states_traj, dones=dones_traj, ep_rets=returns_traj)
    #
    # load_data = np.load('./data.npz')
    # print("load", load_data.files)
    # print("shape", np.shape(load_data['obs']))




