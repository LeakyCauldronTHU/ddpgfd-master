#!/bin/bash
export CUDA_VISIBLE_DEVICES=$1
echo $CUDA_VISIBLE_DEVICES
SEEDS="0 1 2 3 4"
for SEED in $SEEDS
do
    python baselines/run.py --alg=ddpg --env=Ant-v1 --expert_path='dataset/ant.npz' --eval_freq=20 --num_epochs 2000 --traj_limitation 25 --seed $SEED
done