# coding=utf-8
import os
import io
import subprocess
import json
import csv
import numpy as np

from datetime import datetime

code_name = 'ddpgfd-master'

version = "start"

PHILLY_FS = r'"\\scratch2\scratch\Philly\philly-fs\windows\philly-fs.exe"'
# PHILLY_FS = r'"D:\philly-fs.exe"'
cluster = "gcr"
# cluster = "rr1"
vc = "resrchvc"
user = 'v-wancha'
passwd = ''
# proj = '<Your code path under /philly/eu1/pnrsy/your_alias/>'
os.environ["PHILLY_VC"] = vc


def philly_cmd(commandline):
    print("philly_FS", PHILLY_FS)
    print("philly cmd", commandline)
    subprocess.call(r'{0} {1}'.format(PHILLY_FS, commandline), shell=True)

workdir_philly_fs = r"gfs://{0}/{1}/{2}".format(cluster, vc, user)
input_dir = r"/hdfs/resrchvc/v-wancha"

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("workdir_philly_fs", workdir_philly_fs)
print("input_dir", input_dir)
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print()

local_config_folder = r"D:\Mahjong-Models-Pytorch-v{0}\script-bash\config".format(version)
remote_config_folder = r'{0}/models/configs/'.format(workdir_philly_fs)


# local_bash_folder = r"D:\shell" + code_name
local_bash_folder = os.path.join("c:\d\shell", code_name + "-" + cluster)
os.makedirs(local_bash_folder, exist_ok=True)

remote_bash_folder = r'{0}/script-bash/script'.format(workdir_philly_fs)
# remote_bash_linux_folder = r"/hdfs/{0}/{1}/{2}/script-bash/script".format(vc, user, proj)
remote_bash_linux_folder = (r"//philly/" + cluster + "/resrchvc/{0}/shell/" + code_name + "-" + cluster).format(user)
remote_bash_folder = remote_bash_linux_folder

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
# print("local config folder: ", local_config_folder)
# print("remote config folder: ", remote_config_folder)
# print("local bash folder: ", local_bash_folder)
print("remote bash folder: ", remote_bash_folder)
print("remtoe bash linux folder: ", remote_bash_linux_folder)
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print()


def upload_config(filename, submit=False):
    local_config = r'{0}\{1}'.format(local_config_folder, filename)
    print("\n~~~~~~~~~~~~~~~upload_config~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("local config file:", local_config)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
    if submit:
        philly_cmd('-cp {0} {1}/'.format(local_config, remote_config_folder))
    return local_config


def upload_bash(filename, submit=False):
    local_bash = r'{0}\{1}'.format(local_bash_folder, filename) + '.sh'
    remote_bash = r'{0}'.format(remote_bash_folder)

    print("\n~~~~~~~~~~~~~~~upload_bash~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("local bash file: ", local_bash)
    print("remtoe bash file: ", remote_bash)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")

    if submit:
        philly_cmd('-cp {0} {1}'.format(local_bash, remote_bash_folder))
        philly_cmd('-chmod 777 {0}'.format(remote_bash))
    print("philly command line", '-cp {0} {1}'.format(local_bash, remote_bash_folder))
    exit(0)
    return remote_bash


def submit_job(jobname, filename, submit=False):
    remote_bash = r'{0}/{1}'.format(remote_bash_linux_folder, filename)
    print("remote bash", remote_bash)

    head_url = "https://philly/api/submit?"
    CMD = "clusterId={0}&".format(cluster)
    CMD += "registry=phillyregistry.azurecr.io&"
    CMD += "repository=philly/jobs/custom/tensorflow&tag=tf110-py36&"
    CMD += "buildId=0000&clusterId={0}&".format(cluster)
    CMD += "vcId={0}&".format(vc)
    CMD += "rackid=anyConnected&"
    # CMD += "Queue={0}&".format("bonus")
    CMD += "configFile={0}&".format(remote_bash)
    CMD += "minGPUs=1&name={0}&".format(jobname)
    # CMD += "name={0}&".format(jobname)
    CMD += "isdebug=False&ismemcheck=false&isperftrace=false&iscrossrack=false&oneProcessPerContainer=true&dynamicContainerSize=false&numOfContainers=1"
    CMD += "&inputDir={0}&userName={1}&submitCode=p".format(input_dir, user)

    url = head_url + CMD

    print("~~~~~~~~~~~~~~~~~~~~~submit job~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    # print("\n" + url + "\n")
    # print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    # exit(0)
    max_submit_times = 20
    counter = 0
    if submit:
        while counter < max_submit_times:
            print("Is submitting the job for the {} time......".format(counter+1))
            counter += 1
            # os.system(r'curl -k --ntlm --user "{0}:{1}" "{2}"'.format(user, passwd, url))
            # response = os.popen(r'curl -k --ntlm --user "{0}:{1}" "{2}"'.format(user, passwd, url)).readlines()
            response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(url)).readlines()

            content = json.loads(response[0])
            print("content: ", content)
            # if "error" in content:
            #     print("error detected")

            if "jobId" in content:
                print(content["jobId"][12:])
                return content["jobId"][12:]
            else:
                print("re-submit jobs")
                # return "-1"
        if "error" in content:
            print("fail to submit jobs")
            return "-1"
    print("\n~~~~~~~~~~~~~~~~~~finish submit~~~~~~~~~~~~~~~~~~~~~")


def save_bash(config_file_list, file_name):
    bash_file = os.path.join(local_bash_folder, file_name + ".sh")

    command = r"python -m baselines.run --alg=ddpg --env=uav-v0 --num_epochs=15000" \
              r" --seed={0} --expert_path={1} --variance={2} " \
              r"--traj_limitation={3} --cluster={4}".format(config_file_list[0], config_file_list[1], config_file_list[2]
                                                          , config_file_list[3], cluster)

    print("\n~~~~~~~~~~~~~~~ our new bash file ~~~~~~~~~~~~")
    print("commond line", command)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    with io.open(bash_file, "w", newline='\n') as f:
        f.write("pip install --user matplotlib"+"\n")
        f.write("pip install --user setproctitle"+"\n")
        f.write("pip install --user mpi4py"+"\n")
        f.write("pip install --user gym"+"\n")
        f.write("cd ~"+"\n")
        f.write("git clone https://DennisWangWC@bitbucket.org/DennisWangWC/gym-uav.git"+"\n")
        f.write("cd gym-uav"+"\n")
        f.write("pip install --user -e ."+"\n")
        f.write("cd .."+"\n")
        f.write("git clone https://DennisWangWC@bitbucket.org/DennisWangWC/" + code_name + ".git"+"\n")
        f.write("cd " + code_name + "\n")
        f.write("cd baselines" + "\n")
        f.write("pip install --user -e ." + "\n")
        f.write(command)


if __name__ == "__main__":
    submit = True
    counter = 0

    demo = r'//philly/' + cluster + r'/resrchvc/v-wancha/demonstrations/'
    variance = ['0.0', '0.4']
    demo_type = ['uav', 'uav_wrong']
    # demo_type = ['uav_wrong']
    path = []
    key = []
    for v in variance:
        for d in demo_type:
            key.append(d + "_" + v.replace('.', '_'))
            path.append(demo + d + "_" + v.replace('.', '_') + '.npz')
    demo_path = dict(zip(key, path))

    seeds = ['1', '2', '3', '4', '5']
    # seeds = ['3']
    traj_limitation = ['1', '100', '500']

    for see in seeds:
        for dem in demo_type:
            for tra in traj_limitation:
                for var in variance:
                    counter += 1
                    bash_filename = code_name + "_" + "env=uav-v0" + "_" + "see=" + see + "_" + "dem=" + dem + "_" + \
                                    "tra=" + tra + "_" + "var=" + var + ".sh"

                    job_name = bash_filename[:-3]
                    job_name = job_name.replace('.', '_')
                    print("job name", job_name)
                    key = dem + "_" + var.replace('.', '_')
                    save_bash([see, demo_path[key], var, tra], job_name)
                    # if counter > 0:
                    #     jobId = submit_job(job_name, job_name + '.sh', submit)
                        # exit(0)

